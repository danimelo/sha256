#!/usr/bin/env python3
'''
$ pylint danimelo.py 

------------------------------------
Your code has been rated at 10.00/10

$
'''
import hashlib


def to_sha256(string):

    """plain text goes into a sha256 """
    #Unicode-objects must be encoded before hashing
    string_hash = hashlib.sha256(string.encode('utf-8'))
    return string_hash.hexdigest()


def main():

    file = open("DATA.lst")
    data = file.read()
    print(to_sha256(data))
    file.close()  # It's important to close the file when you're done with it


if __name__=="__main__":

	main()


#
#$ cat DATA.lst | python3 maindata.py
# 7f83b1657ff1fc53b92dc18148a1d65dfc2d4b1fa3d677284addd200126d9069
#$
#